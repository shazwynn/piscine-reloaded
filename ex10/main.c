#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_swap(int *a, int *b);

int		main(void)
{
	int a;
	int	b;

	a = 42;
	b = 66;
	ft_swap(&a, &b);
	ft_putchar(a);
	ft_putchar('\n');
	ft_putchar(b);
	ft_putchar('\n');
	return (0);
}
