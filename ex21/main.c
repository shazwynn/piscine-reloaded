#include <stdio.h>

int	*ft_range(int min, int max);

int	main(void)
{
	int	i;
	int len;
	int min;
	int max;
	int	*tab;

	min = -2;
	max = 12;
	i = 0;
	len = max - min;
	tab = ft_range(min, max);
	while (i < len)
	{
		printf("%d\n", tab[i]);
		i++;
	}
	return (0);
}
