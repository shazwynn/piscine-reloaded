#include <stdio.h>

int		ft_count_if(char **tab, int (*f)(char *));

int		ft_strlen(char *str)
{
	int	len;

	len = 0;
	while (str[len])
		len++;
	return (len);
}

int		lensuptwo(char *str)
{
	if (ft_strlen(str) > 2)
		return (1);
	else
		return (0);
}

int		main(int argc, char **argv)
{
	if (argc > 1)
		printf("%d\n", ft_count_if(argv, &lensuptwo));
	return (0);
}
