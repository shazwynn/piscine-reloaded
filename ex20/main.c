#include <stdio.h>

char	*ft_strdup(char *src);

int	main(void)
{
	char	*str;
	char	*dup;

	str = "bonjour";
	dup = ft_strdup(str);
	printf("%s -> %s", str, dup);
	return (0);
}
