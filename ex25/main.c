#include <stdio.h>

void	ft_putchar(char c);
void	ft_putnbr(int nbr);
int		*ft_range(int min, int max);
void	ft_foreach(int *tab, int length, void (*f)(int));

int		main(void)
{
	int	*tab;
	int	len;
	int	i;

	i = 0;
	len = 12 - 5;
	tab = ft_range(5, 12);
	while (i < len)
	{
		printf("%d\n", tab[i]);
		i++;
	}
	printf("-----\n");
	ft_foreach(tab, 12 - 5, &ft_putnbr);
	ft_putchar('\n');
}
