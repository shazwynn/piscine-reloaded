#include <unistd.h>
#include <stdio.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		ft_putchar(str[i]);
		i++;
	}
}

int		ft_strcmp(char *s1, char *s2);

int		main(void)
{
	char	*str;
	char	*str2;

	str = "bonjoura";
	str2 = "bonjourb";
	printf("%s - %s = %d", str, str2, ft_strcmp(str, str2));
	return (0);
}
