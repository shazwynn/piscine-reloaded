#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_is_negative(int n);

int		main(void)
{
	ft_is_negative(12);
	ft_putchar('\n');
	ft_is_negative(15643);
	ft_putchar('\n');
	ft_is_negative(0);
	ft_putchar('\n');
	ft_is_negative(-87);
	ft_putchar('\n');
	return (0);
}
