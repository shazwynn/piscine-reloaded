#include <unistd.h>
#include <stdio.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str);

int		main(void)
{
	ft_putstr("hello world\n");
	return (0);
}
