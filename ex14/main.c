#include <unistd.h>
#include <stdio.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int	ft_sqrt(int nb);

int		main(void)
{
	printf("%d\n", ft_sqrt(-12));
	printf("%d\n", ft_sqrt(0));
	printf("%d\n", ft_sqrt(1));
	printf("%d\n", ft_sqrt(345));
	printf("%d\n", ft_sqrt(111));
	printf("%d\n", ft_sqrt(9));
	printf("%d\n", ft_sqrt(1073741824));
	printf("%d\n", ft_sqrt(2147483647));
	return (0);
}
