#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_ft(int *nbr);

int		main(void)
{
	int number;

	number = 2;
	ft_ft(&number);
	ft_putchar(number);
	ft_putchar('\n');
	return (0);
}
