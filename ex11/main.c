#include <unistd.h>
#include <stdio.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_div_mod(int a, int b, int *div, int *mod);

int		main(void)
{
	int a;
	int	b;
	int	div;
	int mod;

	a = 42;
	b = 5;
	div = 0;
	mod = 0;
	ft_div_mod(a, b, &div, &mod);
	printf("%d / %d = %d, reste %d", a, b, div, mod);
	return (0);
}
