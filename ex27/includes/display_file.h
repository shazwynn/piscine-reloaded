/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_file.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 20:00:55 by algrele           #+#    #+#             */
/*   Updated: 2018/04/03 20:00:59 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DISPLAY_FILE_H
# define DISPLAY_FILE_H

# include <unistd.h>
# include <fcntl.h>

# define BUF_SIZE 100

/*
** DISPLAY
*/

void			ft_putchar(char c);
void			ft_putstr(char *str);
void			ft_putstr_error(char *str);
void			ft_putnbr(int nb);

/*
** READ_FILE
*/

int				ft_display_file(char *file);
unsigned long	count_size(int fd);
int				read_file(int fd, unsigned long size);

#endif
