/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/26 01:52:50 by algrele           #+#    #+#             */
/*   Updated: 2018/02/26 02:38:32 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "display_file.h"

int	main(int argc, char **argv)
{
	if (argc == 1)
		ft_putstr_error("File name missing.\n");
	else if (argc > 2)
		ft_putstr_error("Too many arguments.\n");
	else
		ft_display_file(argv[1]);
	return (0);
}
