/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_file.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 20:01:09 by algrele           #+#    #+#             */
/*   Updated: 2018/04/03 20:02:22 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "display_file.h"

int				ft_display_file(char *file)
{
	int				fd;
	unsigned long	size;

	fd = open(file, O_RDONLY);
	if (fd == -1)
		return (0);
	size = count_size(fd);
	close(fd);
	fd = open(file, O_RDONLY);
	read_file(fd, size);
	return (0);
}

int				read_file(int fd, unsigned long size)
{
	char			str[size + 1];
	unsigned long	i;
	int				j;
	int				ret;
	char			buf[BUF_SIZE];

	i = 0;
	while ((ret = read(fd, buf, BUF_SIZE)))
	{
		j = 0;
		while (j < ret)
			str[i++] = buf[j++];
	}
	str[i] = '\0';
	ft_putstr(str);
	close(fd);
	return (0);
}

unsigned long	count_size(int fd)
{
	char			buf[BUF_SIZE];
	int				ret;
	unsigned long	total;

	total = 0;
	while ((ret = read(fd, buf, BUF_SIZE)))
		total = total + ret;
	return (total);
}
