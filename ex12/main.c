#include <unistd.h>
#include <stdio.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int	ft_iterative_factorial(int nb);

int		main(void)
{
	int	i;

	i = -1;
	while (i <= 14)
	{
		printf("%d\n", ft_iterative_factorial(i));
		i++;
	}
	return (0);
}
